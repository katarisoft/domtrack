package com.katarisoft.domtrack.model;

import java.sql.Date;

import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Entity
@Table(name = "clon")
@Audited
@EntityListeners(AuditingEntityListener.class)

public class Clon {
    @Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "domain")
	private String domain;

	@Column(name = "ip")
	private String ip;
	@Lob
	@Column(name = "file",columnDefinition="TEXT")
	private String file;
	
	@Column(name = "time")
	private String date;
	
	@Column(name = "sistema")
	private String sistema;

	@Column(name = "path")
	private String path;

	@NotNull
	@Valid
	@ManyToOne(cascade = CascadeType.DETACH)
	private Client cliente;


    @Column(name = "created_date", nullable = false, updatable = false)
    @CreatedDate
    private long createdDate;

    @Column(name = "modified_date")
    @LastModifiedDate
    private long modifiedDate;

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getDomain() {
			return domain;
		}

		public void setDomain(String domain) {
			this.domain = domain;
		}

		public String getIp() {
			return ip;
		}

		public void setIp(String ip) {
			this.ip = ip;
		}

		public String getFile() {
			return file;
		}

		public void setFile(String file) {
			this.file = file;
		}

		public String getDate() {
			return date;
		}

		public void setDate(String date) {
			this.date = date;
		}

		public long getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(long createdDate) {
			this.createdDate = createdDate;
		}

		public long getModifiedDate() {
			return modifiedDate;
		}

		public void setModifiedDate(long modifiedDate) {
			this.modifiedDate = modifiedDate;
		}

		public String getSistema() {
			return sistema;
		}

		public void setSistema(String sistema) {
			this.sistema = sistema;
		}

		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}

		public Client getCliente() {
			return cliente;
		}

		public void setCliente(Client cliente) {
			this.cliente = cliente;
		}

		
	    
	
}
