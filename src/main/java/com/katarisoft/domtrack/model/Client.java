package com.katarisoft.domtrack.model;

import java.util.Objects;
import java.util.UUID;

import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "clon")
@Audited
@EntityListeners(AuditingEntityListener.class)

public class Client {
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
	    private long id;
		
		@Column(name = "hash")
		@GeneratedValue(strategy = GenerationType.UUID)
		private String hash;

		@Column(name = "name", nullable=false)
		private String name;
		
		@Column(name = "domain", nullable=false)
		private String domain;

		@Column(name = "ip")
		private String ip;
		
		@Column(name = "script")
		private String script;
		
		@Column(name = "enable", columnDefinition = "boolean default true")
		private boolean enable;
		
	    @Column(name = "created_date", nullable = false, updatable = false)
	    @CreatedDate
	    private long createdDate;

	    @Column(name = "modified_date")
	    @LastModifiedDate
	    private long modifiedDate;


		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getDomain() {
			return domain;
		}
		public void setDomain(String domain) {
			this.domain = domain;
		}
		public String getIp() {
			return ip;
		}
		public void setIp(String ip) {
			this.ip = ip;
		}
		public String getScript() {
			String sc="<script ref=\""+hash+"\" src=\"http://localhost:8084/sendInfo.js\"></script>";
			return sc;
		}
		
		public long getCreatedDate() {
			return createdDate;
		}
		public void setCreatedDate(long createdDate) {
			this.createdDate = createdDate;
		}
		public long getModifiedDate() {
			return modifiedDate;
		}
		public void setModifiedDate(long modifiedDate) {
			this.modifiedDate = modifiedDate;
		}
		public boolean isEnable() {
			return enable;
		}
		public void setEnable(boolean enable) {
			this.enable = enable;
		}
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public String getHash() {
			return hash;
		}

		 public void setHash(String hash) {
			this.hash = hash;
		}
		@PrePersist
	    private void beforeAnyUpdate() {
		        hash=UUID.randomUUID().toString();
	    }
	
	    
}
