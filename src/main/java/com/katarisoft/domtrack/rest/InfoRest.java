package com.katarisoft.domtrack.rest;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.katarisoft.domtrack.dto.MensajeDto;
import com.katarisoft.domtrack.model.Client;
import com.katarisoft.domtrack.model.Clon;
import com.katarisoft.domtrack.repository.ClientRepository;
import com.katarisoft.domtrack.repository.ClonRepository;
import com.katarisoft.domtrack.service.CryptoUtil;
import com.katarisoft.domtrack.service.EmailUtil;

import jakarta.mail.MessagingException;

@RestController
@RequestMapping("info")
public class InfoRest {
	@Autowired
	CryptoUtil cripto;
	@Autowired
	EmailUtil emailUtil;
	@Autowired
	private ClonRepository repository;
	@Autowired
	private ClientRepository clientRepository;

	@CrossOrigin
	  @PostMapping("/")
	  String mensaje(@RequestBody MensajeDto mensaje)  {
		System.out.println(mensaje.getHash());
		System.out.println(mensaje.getMensaje());
		String secret = "test";
		String cipherText = mensaje.getMensaje();

		try {
			String json = cripto.decode(secret, cipherText);
			Gson g = new Gson();
			System.out.println(json);
			Clon clon = g.fromJson(json, Clon.class);
			System.out.println(clon.getDomain());
			   List<Client> c = clientRepository.findByHash(clon.getCliente().getHash());
	           
	            if(c.size()>0) {
	            Client cli = c.get(0);
	            clon.setCliente(cli);
	            repository.save(clon);
	          //  emailUtil.enviamensaje(clon);

	            }
	        
		}catch(InvalidKeyException|NoSuchAlgorithmException|NoSuchPaddingException|InvalidAlgorithmParameterException|IllegalBlockSizeException|

	BadPaddingException 
	//| MessagingException 
	e)
	{
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	return null;

}

}
