package com.katarisoft.domtrack.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.katarisoft.domtrack.model.Client;
import com.katarisoft.domtrack.repository.ClientRepository;
import com.katarisoft.domtrack.repository.ClientRepositoryPage;

@RestController
@RequestMapping("client")
public class ClientRest {
	@Autowired
	private ClientRepository repository;
	@Autowired
	private ClientRepositoryPage repositoryp;
	@CrossOrigin
	@GetMapping("/")
	Page<Client>  all(   @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @RequestParam(defaultValue = "id") String sortBy,
            @RequestParam(defaultValue = "id") String direction
            ) {
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		
        Page<Client> pagedResult = repository.findAll(paging);
        return repository.findAll(paging);
//        if(pagedResult.hasContent()) {
//            return pagedResult.getContent();
//        } else {
//            return new ArrayList<Client>();
//        }
       
	}
	
	@CrossOrigin
	@GetMapping("/{id}")
	Client get(@PathVariable long id) {
		return repository.findById(id).get();
	}
	@CrossOrigin
	@PostMapping("/")
	Client save(@RequestBody Client client) {
		
			repository.save(client);
			
		
		return client;
	}
	@CrossOrigin
	@DeleteMapping("/{id}")
	 void delete(@PathVariable long id) {
		 repository.deleteById(id);
	}
}
