package com.katarisoft.domtrack.rest;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import freemarker.template.Configuration;

import com.katarisoft.domtrack.model.Client;
import com.katarisoft.domtrack.model.Clon;
import com.katarisoft.domtrack.repository.ClientRepository;
import com.katarisoft.domtrack.repository.ClonRepository;
import com.katarisoft.domtrack.service.EmailUtil;
import com.yahoo.platform.yui.compressor.JavaScriptCompressor;

import jakarta.mail.internet.MimeMessage;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ClonRest {
	@Value("${domtrack.hostUrl}")
	private String hostUrl;
	@Autowired
	private ClonRepository repository;
	@Autowired
	private ClientRepository clientRepository;

	 @Autowired     
	 EmailUtil emailUtil;
	@CrossOrigin
	@GetMapping("/")
	  List<Clon> all() {
	    return repository.findAll();
	  }
	  // end::get-aggregate-root[]
	@CrossOrigin
	  @PostMapping("/")
	  String newEmployee(@RequestBody Clon clon) {
		  try {
			  
	            List<Client> c = clientRepository.findByHash(clon.getCliente().getHash());
	           
	            if(c.size()>0) {
	            Client cli = c.get(0);
	            clon.setCliente(cli);
	            repository.save(clon);
	            emailUtil.enviamensaje(clon);

	            }
	        }
	 
	        // Catch block to handle the exceptions
	        catch (Exception e) {
	        	e.printStackTrace();
	            return "Error while Sending Mail";
	        }
		  
	    return "Ok";
	  }
	@GetMapping("/sendInfo.js")
	   public String handle() throws IOException {
	   
		String s1 =FileUtils.readFileToString(ResourceUtils.getFile("./sendInfo.js"), StandardCharsets.UTF_8);
	      return s1.replace("http://localhost:8084/", hostUrl);
	}
	
}
