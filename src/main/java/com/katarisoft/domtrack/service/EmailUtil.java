package com.katarisoft.domtrack.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.katarisoft.domtrack.model.Client;
import com.katarisoft.domtrack.model.Clon;

import freemarker.template.Configuration;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;

@Service
public class EmailUtil {
	@Autowired
	private JavaMailSender emailSender;
	private static final SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Autowired
	Configuration fmConfiguration;

	public void enviamensaje(Clon clon) throws MessagingException {
		MimeMessage mimeMessage = emailSender.createMimeMessage();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		MimeMessageHelper mailMessage = new MimeMessageHelper(mimeMessage, true);

	
		// Setting up necessary details
        mailMessage.setFrom("notificaciones@evidenciainformatica.com.ec");
        mailMessage.setTo("juan.urgilesc@gmail.com");
        Map< String, Object > model = new HashMap<>();
        model.put("cliente", "Juan Mejia");
        model.put("ip", clon.getIp());
        model.put("dominio", clon.getDomain());
        model.put("sistema", clon.getSistema());
        model.put("time", sdf3.format(timestamp.getTime()));
        
        mailMessage.setText(geContentFromTemplate(model), true);
     

        mailMessage.setSubject("Tu sitio ha sido clonado "+timestamp.getTime() );

        // Sending the mail
        emailSender.send(mailMessage.getMimeMessage());
        //return "Mail Sent Successfully...";
	}
	public String geContentFromTemplate(Map< String, Object >model)     { 
        StringBuffer content = new StringBuffer();
 
        try {
            content.append(FreeMarkerTemplateUtils.processTemplateIntoString(fmConfiguration.getTemplate("email-template.flth"), model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content.toString();
    }
}
