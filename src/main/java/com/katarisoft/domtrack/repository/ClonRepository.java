package com.katarisoft.domtrack.repository;

import org.springframework.data.repository.ListCrudRepository;

import com.katarisoft.domtrack.model.Clon;

public interface ClonRepository extends ListCrudRepository<Clon, Long> {

}
