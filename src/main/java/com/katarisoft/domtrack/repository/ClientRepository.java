package com.katarisoft.domtrack.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.katarisoft.domtrack.model.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {

	List<Client>  findByHash(String hash);

}
