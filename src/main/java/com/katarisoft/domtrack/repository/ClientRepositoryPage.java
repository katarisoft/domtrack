package com.katarisoft.domtrack.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.ListCrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.katarisoft.domtrack.model.Client;

public interface ClientRepositoryPage extends PagingAndSortingRepository<Client, Long> {

}
