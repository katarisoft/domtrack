package com.katarisoft.domtrack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DomtrackApplication {

	public static void main(String[] args) {
		SpringApplication.run(DomtrackApplication.class, args);
	}

}
